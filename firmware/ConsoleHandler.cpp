#include "ConsoleHandler.h"
extern "C"{
#include "usb_vcp.h"
}

using namespace std;

ConsoleHandler::ConsoleHandler():
  recv_buf()
{}

ConsoleHandler::~ConsoleHandler(){
}

void ConsoleHandler::pollVCP(){
  usbd_poll(usb_vcp_get_dev());
  while(usb_vcp_avail()>0){
    char byte = usb_vcp_recv_byte();
    if(byte == '\n' || byte == '\r'){
      this->handleLine(recv_buf);
      recv_buf.clear();
    }else{
      recv_buf += byte;
    }
  }
}

void ConsoleHandler::sendLine(string line){
  usb_vcp_send_strn(line.c_str(),line.length());
};
