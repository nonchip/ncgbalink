#pragma once
#include <string>

class ConsoleHandler {
public:
  ConsoleHandler();
  virtual ~ConsoleHandler();
  void pollVCP();
  void sendLine(std::string line);
  virtual void handleLine(std::string line) = 0;
private:
  std::string recv_buf;
};
