#include <stdlib.h>
#include <string>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

extern "C"{
#include "usb_vcp.h"
#include "hardware_cfg.h"
#include "ram_payload.h"
}

#include "ConsoleHandler.h"

using namespace std;

void cpu_setup(void){
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  rcc_periph_clock_enable(LED_PORT_RCC);
  gpio_set_mode(LED_PORT, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, LED_PIN);
}

class Console: public ConsoleHandler {
  virtual void handleLine(string line) override final{
    switch(line[0]){
      case 'm': // set mode
        break;
      case '?': // get status
        break;
      case 's': // send
        break;
    }
  }
};

int main(void)
{
  cpu_setup();
  usb_vcp_init();

  Console * console = new Console();
  while (1){
    console->pollVCP();
  }

  return 0;
}
