# V1

for testing on Hardware V1

## setup

### prerequisites

* V1 hardware assembled & bootloader flashed according the [hardware readme](../hardware/README.md)
* [GNU Embedded Toolchain for Arm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) `cross-arm-none-eabi`
* GNU Make
* [texane stlink](https://github.com/texane/stlink)

### building

Preparing libOpenCM3:

```
git submodule init
cd libopencm3
make
cd ..
```

Building the firmware:

```
make
```

### firmware flashing

* make sure `BOOT0`/`BOOT1` jumpers are set to `0`
* connect USB, flash with `st-flash main.bin 0x8000000`
