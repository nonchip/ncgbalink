# V1

breadboard test setup w/ "blue pill"

## used hardware

* generic GBAlink breakout
* generic STM32F1 "blue pill" board (one of those chinesy types with the fixed USB)
* stlink

## assembly

using one of those cheap chinese cables with loads of pins and middle socket missing:

```
pinout:

big plug         small plug     1 VCC 3V3
 __.-.__          __.-.__       2 SO
/   3 5 \        /   3 5 \      3 SI
\___4_6_/        \_2_4___/      4 SD
                                5 SC
                                6 GND

internal connections:

big plug   small plug
(slave)     (master)
   3 SI        2 SO
   4 SD        4 SD
   5 SC        5 SC
   6 GND       3 SI
```

connect the small plug (master) to the GBA, connect the big plug (slave) to the breakout.

all on a breadboard:

```
    big plug  |  bluepill
    ----------+----------
         VCC  |  (NC)
          SO  |  (NC)
          SI  |  A2
          SD  |  A1
          SC  |  A0
         GND  |  GND
```
