#include "utils.h"

uint8_t getNibble(uint32_t num, int which) {
    return (num >> (which * 4)) & 0xF;
}

uint32_t setNibble(uint32_t num, uint8_t nibble, int which) {
    return (num & ~(0xF << (which * 4))) | (nibble << (which * 4));
}
