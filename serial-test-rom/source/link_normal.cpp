#include "link.h"
#include <gba_sio.h>
#include <stdexcept>

using namespace std;

LinkNormal::LinkNormal(bool master, bool highspeed, bool inactive_so):
  Link(),
  master(master),
  highspeed(highspeed),
  inactive_so(inactive_so)
{
  REG_RCNT = R_NORMAL;
}

bool LinkNormal::isMaster(){
  return master;
}

bool LinkNormal::isReady(){
  return (REG_SIOCNT & SIO_RDY) != 0;
}

bool LinkNormal::isActive(){
  return (REG_SIOCNT & SIO_START) != 0;
}

void LinkNormal::control(bool active){
  REG_SIOCNT = ( master?      SIO_CLK_INT  :0)
             | ( highspeed?   SIO_2MHZ_CLK :0)
             | ( inactive_so? SIO_SO_HIGH  :0)
             | ( active?      SIO_START    :0)
             | SIO_IRQ;
}

// 8bit

void LinkNormal8::setData(uint8_t data){
  REG_SIODATA8 = data;
}

uint8_t LinkNormal8::getData(){
  return REG_SIODATA8;
}

void LinkNormal8::dispatch_cb(){
  if(callback == NULL) return;
  uint8_t args[] = { getData() };
  callback(args);
}

// 32bit

void LinkNormal32::setData(uint32_t data){
  REG_SIODATA32 = data;
}

uint32_t LinkNormal32::getData(){
  return REG_SIODATA32;
}

void LinkNormal32::dispatch_cb(){
  if(callback == NULL) return;
  uint32_t args[] = { getData() };
  callback(args);
}
