#include "link.h"
#include <gba_interrupt.h>
#include <stdexcept>

using namespace std;

void Link::irq_handler(void){
  if(irq_dispatch_target==NULL) return;
  irq_dispatch_target->dispatch_cb();
}

Link::Link(){
  if(irq_dispatch_target!=NULL) throw logic_error("more than one Link instantiated");
  Link::irq_dispatch_target = this;
  irqSet(IRQ_SERIAL,Link::irq_handler);
  irqEnable(IRQ_SERIAL);
}

Link::~Link(){
  irqDisable(IRQ_SERIAL);
  Link::irq_dispatch_target=NULL;
}

