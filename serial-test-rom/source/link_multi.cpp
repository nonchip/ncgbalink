#include "link.h"
#include <gba_sio.h>
#include <stdexcept>

using namespace std;

LinkMulti::LinkMulti(int bauds):
  Link(),
  bauds(bauds)
{
  REG_RCNT = R_MULTI;
}

void LinkMulti::setData(uint16_t data){
  REG_SIOMLT_SEND = data;
}

uint16_t* LinkMulti::getData(){
  static uint16_t data[4] = {
    REG_SIOMULTI0,
    REG_SIOMULTI1,
    REG_SIOMULTI2,
    REG_SIOMULTI3
  };
  return data;
}

void LinkMulti::control(bool active){
  REG_SIOCNT = (bauds&0x3) | ( isMaster() && (!isError()) && active ? SIO_START : 0 ) | SIO_IRQ;
}


bool LinkMulti::isMaster(){
  return (REG_SIOCNT & SIO_RDY) == 0; // bit 2, not "ready" here
}

bool LinkMulti::isActive(){
  return (REG_SIOCNT & SIO_START) != 0;
}

int LinkMulti::getID(){
  return (REG_SIOCNT & 0x30) >> 4; // bits 4-5
}

bool LinkMulti::isError(){
  return (REG_SIOCNT & 0x40) != 0; // bit 6
}

void LinkMulti::dispatch_cb(){
  if(callback == NULL) return;
  callback(getData());
}
