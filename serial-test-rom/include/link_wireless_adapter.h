#include "link.h"

class LinkWirelessAdapter: public LinkNormal32 {
  // void callback(uint8_t rr, uint32_t data[rr])
public:
  virtual void login();
  virtual void command(uint8_t cc, uint8_t pp, uint32_t params[]);
protected:
  virtual void dispatch_cb() override; // doesn't actually callback() until cmd is complete
private:
  uint8_t rr;
  static uint32_t responses[0xff];
};
