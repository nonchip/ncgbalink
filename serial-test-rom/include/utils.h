#pragma once
#include <stdlib.h>

uint8_t getNibble(uint32_t num, int which);
uint32_t setNibble(uint32_t num, uint8_t nibble, int which);
