#pragma once
#include <stdint.h>

class Link {
public:
  enum {
    BAUD_9600,
    BAUD_38400,
    BAUD_57600,
    BAUD_115200
  };
  Link();
  ~Link();
  virtual bool isMaster() = 0;
  virtual bool isActive() = 0;
  virtual void control(...) = 0;
  virtual void setData(...) = 0;
  void (*callback)(void*);
protected:
  virtual void dispatch_cb() = 0;
private:
  static void irq_handler(void);
  static Link * irq_dispatch_target;
};


class LinkNormal: public Link {
public:
  LinkNormal(bool master = false, bool highspeed = false, bool inactive_so = false);
  virtual bool isMaster() override;
  virtual bool isReady();
  virtual bool isActive() override;
  virtual void control(bool active);
private:
  const bool master, highspeed, inactive_so;
  const static bool length;
};

class LinkNormal8: public LinkNormal {
  // void callback(uint8_t data)
public:
  virtual void setData(uint8_t data);
  virtual uint8_t getData();
protected:
  virtual void dispatch_cb() override;
private:
  const static bool length = false;
};

class LinkNormal32: public LinkNormal {
  // void callback(uint32_t data)
public:
  virtual void setData(uint32_t data);
  virtual uint32_t getData();
protected:
  virtual void dispatch_cb() override;
private:
  const static bool length = true;
};


class LinkMulti: public Link {
  // void callback(uint16_t data[4])
public:
  LinkMulti(int bauds);
  virtual void setData(uint16_t data);
  virtual uint16_t* getData();
  virtual bool isMaster() override;
  virtual bool isActive() override;
  virtual int getID();
  virtual bool isError();
  virtual void control(bool active);
protected:
  virtual void dispatch_cb() override;
private:
  int bauds;
};


class LinkUART: public Link {
public:
  LinkUART(int bauds, bool cts, bool odd, bool eight, bool fifo, bool parity);
  virtual void setData(uint8_t data);
  virtual uint8_t getData();
  virtual bool isError();
  virtual bool isSendFull();
  virtual bool isRecvEmpty();
  virtual void control(bool send, bool recv);
protected:
  virtual void dispatch_cb() override;
private:
  int bauds;
  bool cts, odd, eight, fifo, parity;
};


class LinkJoybus: public Link {
public:
  LinkJoybus();
  virtual void ackReset();
  virtual void ackRecv();
  virtual void ackSend();
  virtual bool isRecv();
  virtual bool isSend();
  virtual uint32_t getData();
  virtual void setData(uint32_t data);
protected:
  virtual void dispatch_cb() override;
};


class LinkGPIO: public Link {
public:
  LinkGPIO();
  virtual void control(bool sc, bool sd, bool si, bool so);
  virtual void setData(uint8_t data);
  virtual uint8_t getData();
protected:
  virtual void dispatch_cb() override;
private:
  bool sc, sd, si, so;
};
