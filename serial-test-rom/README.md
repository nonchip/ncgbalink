# setup

* get [devkitarm/devkitpro](https://devkitpro.org/wiki/Getting_Started)
* get [tonclib](https://www.coranac.com/projects/tonc/) (inside "example code")
    - build it (`make` inside `code/tonclib`)
    - add paths to the environment (`/etc/profile.d/devkit-env.sh`):
        + `export LIBGBA=/opt/devkitpro/libgba` (from devkitpro, required by tonc)
        + `export TONCLIB=/home/kyra/electronics/gameboy_stuff/gba-tonc/code/tonclib` (from tonc, required by makefile)
* run `make`
    - this makes a multiboot "rom" called `serial-test-rom_mb.gba`, which e.g. the EZflash Omega cart automatically boots into RAM and can subsequently be yanked, or you could load it using a multiboot loader (such as the one developed as part of this project)
    - to make a "normal" rom file, run `make TARGET=serial-test-rom`, which produces `serial-test-rom.gba` instead.
